[![PyPI status](https://img.shields.io/pypi/status/frkl.jinja2schema.svg)](https://pypi.python.org/pypi/frkl.jinja2schema/)
[![PyPI version](https://img.shields.io/pypi/v/frkl.jinja2schema.svg)](https://pypi.python.org/pypi/frkl.jinja2schema/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/frkl.jinja2schema.svg)](https://pypi.python.org/pypi/frkl.jinja2schema/)
[![Pipeline status](https://gitlab.com/frkl/frkl.jinja2schema/badges/develop/pipeline.svg)](https://gitlab.com/frkl/frkl.jinja2schema/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# frkl.jinja2schema

*Type inference for Jinja2 templates*

---

This repository is a fork of Anton Romanovich's https://github.com/aromanovich/jinja2schema, which does not seem to have
any activitiy anymore.

Original version information:

[Demo](http://jinja2schema.aromanovich.ru/) \|
[Documentation](https://jinja2schema.readthedocs.io/) \|
[GitHub](https://github.com/aromanovich/jinja2schema) \|
[PyPI](https://pypi.python.org/pypi/jinja2schema)

---

## Description


A library that provides a heuristic type inference algorithm for
[Jinja2](http://jinja.pocoo.org/docs/) templates.

``` {.python}
>>> from jinja2schema import infer, to_json_schema
>>> s = infer('{{ (x.a.b|first).name }}')
>>> s
{'x': {'a': {'b': [{'name': <scalar>}]}}

>>> s = infer('''
... {% for x in xs %}
...   {{ x }}
... {% endfor %}
''')
>>> s
{'xs': [<scalar>]}
>>> to_json_schema(s)
{
    'type': 'object',
    'required': ['xs'],
    'properties': {
        'xs': {
            'type': 'array'
            'title': 'xs',
            'items': {
                'title': 'x',
                'anyOf': [
                    {'type': 'string'},
                    {'type': 'number'},
                    {'type': 'boolean'},
                    {'type': 'null'}
                ],
            },
        }
    }
}
```

# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'frkl.jinja2schema' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 jinja2schema
    git clone https://gitlab.com/frkl/frkl.jinja2schema
    cd <jinja2schema_dir>
    pyenv local jinja2schema
    pip install -e .[all-dev]
    pre-commit install


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
