# -*- coding: utf-8 -*-
"""
Some py2/py3 compatibility support based on a stripped down
version of six so we don't have to depend on a specific version
of it.
"""

# flake8: noqa

import sys


unichr = chr
range_type = range
text_type = str
string_types = (str,)

iterkeys = lambda d: iter(d.keys())
itervalues = lambda d: iter(d.values())
iteritems = lambda d: iter(d.items())

from io import BytesIO, StringIO

NativeStringIO = StringIO


def reraise(tp, value, tb=None):
    if value.__traceback__ is not tb:
        raise value.with_traceback(tb)
    raise value


ifilter = filter
imap = map
izip = zip
intern = sys.intern

get_next = lambda x: x.__next__
