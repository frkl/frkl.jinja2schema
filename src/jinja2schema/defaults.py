# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs


jinja2schema_app_dirs = AppDirs("jinja2schema", "frkl")

if not hasattr(sys, "frozen"):
    JINJA2SCHEMA_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `jinja2schema` module."""
else:
    JINJA2SCHEMA_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "jinja2schema"
    )  # type: ignore
    """Marker to indicate the base folder for the `jinja2schema` module."""

JINJA2SCHEMA_RESOURCES_FOLDER = os.path.join(
    JINJA2SCHEMA_MODULE_BASE_FOLDER, "resources"
)
