# -*- coding: utf-8 -*-
import click
from . import __version__


@click.command()
@click.pass_context
def cli(ctx):

    print(f"version: {__version__}")


if __name__ == "__main__":
    cli()
