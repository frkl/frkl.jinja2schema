# -*- coding: utf-8 -*-
"""

jinja2schema
============

Type inference for Jinja2 templates.

See https://jinja2schema.readthedocs.io/ for documentation.

:copyright: (c) 2017 Anton Romanovich
:license: BSD

"""

# flake8: noqa

import io
import os

from pkg_resources import DistributionNotFound, get_distribution

from .config import Config
from .core import (
    parse,
    infer,
    infer_from_ast,
    to_json_schema,
    JSONSchemaDraft4Encoder,
    StringJSONSchemaDraft4Encoder,
)
from .exceptions import (
    InferException,
    MergeException,
    InvalidExpression,
    UnexpectedExpression,
)

__title__ = "jinja2schema"
__author__ = "Anton Romanovich"
__license__ = "BSD"
__copyright__ = "Copyright 2017 Anton Romanovich"

try:
    # Change here if project is renamed and does not equal the package name
    dist_name = __name__
    __version__ = get_distribution(dist_name).version
except DistributionNotFound:
    try:
        version_file = os.path.join(os.path.dirname(__file__), "version.txt")

        if os.path.exists(version_file):
            with io.open(version_file, encoding="utf-8") as vf:
                __version__ = vf.read()
        else:
            __version__ = "unknown"

    except (Exception):
        pass

    if __version__ is None:
        __version__ = "unknown"
finally:
    del get_distribution, DistributionNotFound
